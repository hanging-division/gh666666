package com.lfg.qr_day1.mapper;

import com.lfg.qr_day1.domain.TestGlhkUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author liufaguang
* @description 针对表【test_glhk_user】的数据库操作Mapper
* @createDate 2023-09-15 14:45:12
* @Entity com.lfg.qr_day1.domain.TestGlhkUser
*/
@Mapper
public interface TestGlhkUserMapper extends BaseMapper<TestGlhkUser> {

}




