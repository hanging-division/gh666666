package com.lfg.qr_day1.mapper;

import com.lfg.qr_day1.domain.StuClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author liufaguang
* @description 针对表【stu_class(班级)】的数据库操作Mapper
* @createDate 2023-09-15 11:59:13
* @Entity com.lfg.qr_day1.domain.StuClass
*/
@Mapper
public interface StuClassMapper extends BaseMapper<StuClass> {

}




