package com.lfg.qr_day1.mapper;

import com.lfg.qr_day1.domain.TestGlhkDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author liufaguang
* @description 针对表【test_glhk_dept】的数据库操作Mapper
* @createDate 2023-09-15 14:45:12
* @Entity com.lfg.qr_day1.domain.TestGlhkDept
*/
@Mapper
public interface TestGlhkDeptMapper extends BaseMapper<TestGlhkDept> {

}




