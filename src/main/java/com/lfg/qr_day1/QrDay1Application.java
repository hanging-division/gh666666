package com.lfg.qr_day1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class QrDay1Application {

    public static void main(String[] args) {
        SpringApplication.run(QrDay1Application.class, args);
    }

}
